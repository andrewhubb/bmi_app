import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new HomeState();
  }
}
class HomeState extends State<Home>{
  final _ageController = new TextEditingController();
  final _heightController = new TextEditingController();
  final _weightController = new TextEditingController();

  String _bmiString = "";
  String _bmiCategory = "";
  Color _bmiCategoryColor = Colors.black;
  bool visible =  false;
  double result = 0.0;



void _calculateBMI() {
  double height = 0.0;
  double weight = 0.0;
  int age = 0;
  setState(() {
    if (_ageController.text.isNotEmpty) {
      age = int.parse(_ageController.text);
    }
    if (_heightController.text.isNotEmpty) {
      height = double.parse(_heightController.text);
    }
    if (_weightController.text.isNotEmpty) {
      weight = double.parse(_weightController.text);
    }
    if(age > 0 && height > 0.0 && weight > 0.0) {
      result = double.parse(((weight/(height*height)) * 10000).toStringAsFixed(1));
      if (age>17) {
      _bmiString = 'Your BMI: ${result.toStringAsFixed(1)}';
      visible = true;
      if (result < 18.5) {
        _bmiCategory = "Underweight";
        _bmiCategoryColor = Colors.red;
      } else {
        if (result >= 18.5 && result <= 24.9) {
          _bmiCategory = "Normal Weight";
          _bmiCategoryColor = Colors.green;
        } else {
          if (result >= 25.0 && result <= 29.9) {
            _bmiCategory = "Overweight";
            _bmiCategoryColor = Colors.deepOrange;
          } else {
            _bmiCategory = "Obese";
            _bmiCategoryColor = Colors.red;
          }
        }
      }
    } else {
      _bmiCategory = "You don't need to worry about BMI";
      _bmiCategoryColor = Colors.indigo;
      visible = false;
    }
    } else {
      _bmiCategory = "Incorrect / Missing Info, please complete";
      _bmiCategoryColor = Colors.red;
    }
  });
}



  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("BMI"),
        centerTitle: true,
        backgroundColor: Colors.pink,
      ),
      body: new Container(
        alignment: Alignment.topCenter,
        child: new GestureDetector(
          onTap:() {
              FocusScope.of(context).requestFocus(new FocusNode());
          },
        child: new ListView(
          children: <Widget>[
            new Padding(padding: const EdgeInsets.all(5)),
            new Image.asset('images/bmilogo.png',
              width: 120,
              height: 95,
            ),
            new Padding(padding: const EdgeInsets.all(5)),
            new Container(
              width: 380,
              height: 250,
              margin: const EdgeInsets.all(10),
              color: Colors.grey.shade300,
              child: new Column(
                children: <Widget>[
                  new TextField(
                    controller: _ageController,
                    keyboardType: TextInputType.number,
                    decoration: new InputDecoration(
                      labelText: 'Age',
                      hintText: 'e.g. 45',
                      icon: new Icon(Icons.person_outline),
                      ),
                    ),
                    new TextField(
                      controller: _heightController,
                      keyboardType: TextInputType.number,
                      decoration: new InputDecoration(
                        labelText: 'Height in centimetres.',
                        hintText: 'e.g. 175',
                        icon: new Icon(Icons.equalizer),
                      ),
                  ),
                new TextField(
                    controller: _weightController,
                    keyboardType: TextInputType.number,
                    decoration: new InputDecoration(
                      labelText: 'Weight in Kilograms.',
                      hintText: 'e.g. 84.4',
                      icon: new Icon(Icons.line_weight),
                        ),
                    ),
                  new Padding(padding: const EdgeInsets.all(5)),
                  new Center(
                  child: new RaisedButton(
                      onPressed: (_calculateBMI),
                      color: Colors.pink,
                      child: new Text('Calculate',
                          style: new TextStyle(
                              color:Colors.white,
                              fontSize: 16,
                              fontWeight: FontWeight.w400
                          ))
                      ),
                    ),
                    ],
                  ), //End of Input Fields
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new Padding(padding: const EdgeInsets.all(5)),
                    new Text(visible ? _bmiString:"",
                    style: new TextStyle(
                      color: Colors.indigoAccent,
                      fontSize: 29,
                      fontWeight: FontWeight.w700,
                      fontStyle: FontStyle.italic
                    ),),
                    new Padding(padding: const EdgeInsets.all(10)),
                    new Text(_bmiCategory,
                    textAlign: TextAlign.center,
                    style: new TextStyle(
                      color: _bmiCategoryColor,
                      fontSize: 32,
                      fontWeight: FontWeight.w700
                    ))
                  ],
                )
                ],
              ),
            ),
          ),
        );
    }
}

